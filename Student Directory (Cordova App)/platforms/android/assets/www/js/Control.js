
var studentCounter = 0;
var StuImage,StuVocal,StuVideo;
var Student = [];


function stuInfoDialogBox(recv){
    $(document).ready(function(){
        $('.info').addClass('infoPop');
    });
    if(recv || recv == '0'){
        document.getElementById('displayName').innerHTML = Student[recv].name || 'Null';
        document.getElementById('displayEmail').value = Student[recv].email || 'Null';
        document.getElementById('displayContact').value = Student[recv].contact || 'Null';
        document.getElementById('displayImage').src = Student[recv].images || 'Null';
        document.getElementById('displayVocalPath').value = Student[recv].vocal || 'Null';
        document.getElementById('displayVideoPath').value = Student[recv].video || 'Null';
    }
}

function addUserToDatabase(){
    var sName = document.getElementById('name').value  || 'Null';
    var sEmail = document.getElementById('email').value;
    var sContact = document.getElementById('contact').value;
    var sImage = StuImage;
    var sVocal = StuVocal;
    var sVideo = StuVideo;

    Student[studentCounter] = {
        name : sName,
        email : sEmail,
        contact : sContact,
        images : sImage,
        vocal : sVocal,
        video : sVideo
    }
}

function enrollNow(){

    addUserToDatabase();

    var span = document.createElement('span');
    span.setAttribute('class', 'badge spanClose');
    span.appendChild(document.createTextNode("X"));

    var li = document.createElement('li');
    li.setAttribute('class','btn ListItem list-group-item');
    li.setAttribute('onclick','stuInfoDialogBox('+studentCounter+')');
    li.appendChild(document.createTextNode(Student[studentCounter].name));
    li.appendChild(span);



    var ul = document.getElementById('userList');
    ul.appendChild(li);

    studentCounter++;
    $(document).ready(function(){
        $('.formBox').removeClass('formBoxAppear');
    });
}
